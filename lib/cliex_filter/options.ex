defmodule CliexFilter.Options do
  use CliexFilter.Types

  @moduledoc ~S"""
  Implements a struct representing the parsed command line arguments
  """

  defstruct start_rgx: nil,
            end_rgx: nil,
            include_start: false,
            include_end: false,
            replace: nil,
            replace_all: nil

  @type t :: %__MODULE__{
          start_rgx: maybe(Regex.t()),
          end_rgx: maybe(Regex.t()),
          include_start: boolean(),
          include_end: boolean(),
          replace: maybe(Regex.t()),
          replace_all: maybe(Regex.t())
        }

  @type t? :: either(t(), binary())

  @spec from_args(binaries()) :: t?
  def from_args(args) do
    %__MODULE__{}
    |> _parse_args(args)
  end

  @spec _add_start_and_end(binary(), t()) :: t?()
  defp _add_start_and_end(arg, myself) do
    cond do
      myself.end_rgx -> {:error, "Must not have a third positional or undefined keyword parameter #{arg}"}
      myself.start_rgx -> {:ok, %{myself | end_rgx: arg}}
      true -> {:ok, %{myself | start_rgx: arg}}
    end
  end

  @options_rgx ~r{
    \\A -- 
    (?: (?<flag>include-both|include-end|include-start|replace|replace-all) )
    \\z
  }x

  @spec _check_arg(binary(), t()) :: t?()
  defp _check_arg(arg, myself) do
    case Regex.named_captures(@options_rgx, arg) do
      nil -> _add_start_and_end(arg, myself)
    end
  end

  @spec _check_end(t()) :: t?()
  defp _check_end(myself) do
    if myself.end_rgx do
      case Regex.compile(myself.end_rgx) do
        {:ok, compiled} ->
          {:ok, %{myself | end_rgx: compiled}}

        {:error, {msg, pos}} ->
          {:error,
           "Second positional parameter, expected to be <end-rgx>, is not a legal regex at position #{pos}, #{msg}"}
      end
    else
      {:error, "Missing second positional parameter, expected to be <end-rgx>"}
    end
  end

  @spec _check_start(t()) :: t?()
  defp _check_start(myself) do
    if myself.start_rgx do
      case Regex.compile(myself.start_rgx) do
        {:ok, compiled} ->
          {:ok, %{myself | start_rgx: compiled}}

        {:error, {msg, pos}} ->
          {:error,
           "First positional parameter, expected to be <start-rgx>, is not a legal regex at position #{pos}, #{msg}"}
      end
    else
      {:error, "Missing first positional parameter, expected to be <start-rgx>"}
    end
  end

  @spec _check_result(t()) :: t?()
  defp _check_result(myself) do
    with {:ok, checked} <- _check_result_missing(myself) do
      with {:ok, perfect} <- _check_result_coherent(checked) do
        {:ok, perfect}
      end
    end
  end

  @spec _check_result_coherent(t()) :: t?()
  def _check_result_coherent(myself) do
    {:ok, myself}
  end

  @spec _check_result_missing(t()) :: t?()
  def _check_result_missing(myself) do
    with {:ok, okstart} <- _check_start(myself) do
      _check_end(okstart)
    end
  end

  @spec _parse_args(t(), binaries()) :: t?
  defp _parse_args(myself, args)
  defp _parse_args(myself, []), do: _check_result(myself)

  defp _parse_args(myself, [arg | rest]) do
    with {:ok, parsed} <- _check_arg(arg, myself) do
      _parse_args(parsed, rest)
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
