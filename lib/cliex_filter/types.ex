defmodule CliexFilter.Types do
  @moduledoc false

  defmacro __using__(_opts) do
    quote do

      @type binaries :: list(binary())
      @type binary? :: maybe(binary())

      @type color_list :: list(binary() | atom())

      @type either(success_t, error_t) :: {:ok, success_t} | {:error, error_t}

      @type maybe(t) :: nil | t

      @type name_value_pair_t :: {binary(), any()}

      @spec map_either(either(success_t, error_t), (success_t -> transformed_t)) :: either(transformed_t, error_t) when
      success_t: any(), error_t: any(), transformed_t: any()
      def map_either(either_value, mapper) do
        with {:ok, wrapped} <- either_value do
          {:ok, mapper.(wrapped)}
        end
      end
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
