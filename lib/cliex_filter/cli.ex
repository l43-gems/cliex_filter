defmodule CliexFilter.Cli do
  use CliexFilter.Types

  alias ExAequo.Color
  alias CliexFilter.Options

  @moduledoc ~S"""
  The escript main module
  """

  @spec main(binaries()) :: :ok
  def main(argv)

  def main([]) do
    Color.putc([:yellow, "usage:"], :stderr)
    Color.putc(usage(), :stderr)
  end

  def main(["-v" | _]), do: version()
  def main(["--version" | _]), do: version()

  def main(["-h" | _]), do:
    Color.putc(usage(), :stderr)
  def main(["--help" | _]), do:
    Color.putc(usage(), :stderr)

  def main(args, stream \\ nil) do
    options = Options.from_args(args)
    Color.putc(["Coming spoon"])
  end

  defp run(options, stream) do
    # (stream || IO.stream(:stdio, :line))
    # |> Mapper.map(compiled, context)
    # |> Enum.each(&IO.puts/1)
  end

  @spec release_date() :: binary()
  defp release_date do
    Application.fetch_env!(:cliex_map, :release_date)
  end

  @spec usage() :: color_list()
  defp usage do
    [
      :white,
      "  cliex_filter",
      :cyan,
      " -v|--version",
      :white,
      " . . . . . . . . . . shows version and release date\n",
      :white,
      "  cliex_filter",
      :cyan,
      " -h|--help",
      :white,
      "  . . . . . . . . . . . shows this help", nl(2),
      "  cliex_filter",
      :cyan,
      " [options]", :green, " <start-rgx> <end-rgx>", nl(2),
      "    Filter lines from stdin, such that only lines between lines matching ", :green, "<start-rgx>", :white, " and ", :green, "<end-rgx>",
      nl(),
      "    the following ", :cyan, "options", :white, " can modify this behavior as follows", nl(2),
      :cyan, "               --replace=<rgx>", :white, " . . . . . . . . ",
      "the first occurence of ", :cyan, "<rgx>", :white, " is deleted from each line\n",
      :cyan, "               --replace-all=<rgx>", :white, " . . . . . . ",
      "all occurences of", :cyan, "<rgx>", :white, " is deleted from each line\n",
      :cyan, "               --include-both", :white, "  . . . . . . . . ",
      "lines that match ", :green, "<start-rgx>", :white, " and ", :green, "<end-rgx>", :white, " are included into the output\n",
      :cyan, "               --include-end", :white, " . . . . . . . . . ",
      "lines that match ", :green, "<end-rgx>", :white, " are included into the output\n",
      :cyan, "               --include-start", :white, " . . . . . . . . ",
      "lines that match ", :green, "<start-rgx>", :white, " are included into the output", nl(2),
      "More information can be found here: ",
      :blue,
      "      https://hexdocs.pm/cliex_filter/CliexFilter.html"
      ]
      |> List.flatten
  end

  @spec version() :: :ok
  defp version() do
    with {:ok, version_lst} <- :application.get_key(:cliex_map, :vsn) do
      Color.putc(["cliex_map version: ", :cyan, to_string(version_lst), "(#{release_date()})"])
    end
  end

  @spec nl(non_neg_integer(), atom()) :: color_list
  defp nl(n \\ 1, color \\ :white) do
    [
      color |
      ["\n"]
      |> Stream.cycle
      |> Enum.take(n)
    ]
  end
end
# SPDX-License-Identifier: Apache-2.0
