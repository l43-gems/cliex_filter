defmodule Test.Options.OptionsFromArgsTest do
  use ExUnit.Case
  alias CliexFilter.Options
  import Options, only: [from_args: 1]

  describe "errors" do
    test "no arguments" do
      assert from_args([]) ==
               {:error, "Missing first positional parameter, expected to be <start-rgx>"}
    end

    test "one argument" do
      assert from_args(["."]) ==
               {:error, "Missing second positional parameter, expected to be <end-rgx>"}
    end

    test "three arguments" do
      assert from_args(~w[one two three]) ==
               {:error, "Must not have a third positional or undefined keyword parameter three"}
    end

    test "bad start rgx" do
      assert from_args(["(."]) ==
               {:error, "First positional parameter, expected to be <start-rgx>, is not a legal regex at position 2, missing )"}
    end

    test "bad end rgx" do
      assert from_args(~w[.. a(.]) ==
        {:error, "Second positional parameter, expected to be <end-rgx>, is not a legal regex at position 3, missing )"}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
